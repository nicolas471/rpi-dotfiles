#!/bin/bash

# install packages
# For rpi1 (and rpizero) install from raspbian repositories
sudo apt install -y apt-transport-https ca-certificates gnupg2 software-properties-common vim fail2ban ntfs-3g zsh python3-pip git

# Once installed git, intialize repo and add origin
git init .
git remote add origin git@gitlab.com:nicolas471/rpi-dotfiles.git
git fetch origin; git pull origin master

# For rpi2 or above try this:
curl -sSL https://get.docker.com |sh

# Get oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

